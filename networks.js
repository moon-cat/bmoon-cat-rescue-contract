require('dotenv').config();
const HDWalletProvider = require('truffle-hdwallet-provider');

module.exports = {
  networks: {
    development: {
      protocol: 'http',
      host: 'localhost',
      port: 8545,
      gas: 5000000,
      gasPrice: 5e9,
      networkId: '*',
    },
    BSCTestnet: {
      provider: function(){
        return new HDWalletProvider(
            process.env.TESTNET_PRIVATE_KEYS.split(','),
            `${process.env.TESTNET_RPC_END_POINT}`
        )
      },
      gas: 7000000,
      gasPrice: 25000000000,
      network_id: 97
    },
    BSCMainnet: {
      provider: function(){
        return new HDWalletProvider(
            process.env.MAINNET_PRIVATE_KEYS.split(','),
            `${process.env.MAINNET_RPC_END_POINT}`
        )
      },
      gas: 7000000,
      gasPrice: 20000000000,
      network_id: 56
    },
  },
};
